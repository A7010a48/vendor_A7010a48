PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/lenovo/A7010a48/vendor/bin,$(TARGET_COPY_OUT_VENDOR)/bin)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/lenovo/A7010a48/vendor/lib,$(TARGET_COPY_OUT_VENDOR)/lib)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/lenovo/A7010a48/vendor/lib64,$(TARGET_COPY_OUT_VENDOR)/lib64)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/lenovo/A7010a48/vendor/etc,$(TARGET_COPY_OUT_VENDOR)/etc)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/lenovo/A7010a48/vendor/firmware,$(TARGET_COPY_OUT_VENDOR)/firmware)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/lenovo/A7010a48/vendor/thh,$(TARGET_COPY_OUT_VENDOR)/thh)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/lenovo/A7010a48/etc,system/etc)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/lenovo/A7010a48/usr,system/usr)


